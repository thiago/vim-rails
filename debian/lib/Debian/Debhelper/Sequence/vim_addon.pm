#! /usr/bin/perl
# debhelper sequence file for vim addons

use warnings;
use strict;
use Debian::Debhelper::Dh_Lib;

insert_after("dh_auto_install", "dh_vim_addon");

1;
